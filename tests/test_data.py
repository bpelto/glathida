from setup import *

if not package.valid:
  pytest.skip("Skipping data tests", allow_module_level=True)

def test_data_against_datapackage_json():
  if report['warnings']:
    print('Warnings [' + str(len(report['warnings'])) + ']')
    for warning in report['warnings']:
      print(warning)
  else:
    print('Warnings [none]')
  if report['valid']:
    print('Valid data [true]')
  else:
    for table in report['tables']:
      if table['valid']:
        print('Valid ' + table['source'] + ' [true]')
      else:
        print('Valid ' + table['source'] + ' [false]')
        print(goodtables.json.dumps(table['errors']))
    print('Valid data [false]')
  assert report['valid']
