# Glacier Thickness Database (GlaThiDa)

This dataset adheres to the Frictionless Data [Tabular Data Package](https://frictionlessdata.io/specs/tabular-data-package/) specification. All metadata is provided in `datapackage.json`. This README is automatically generated from the contents of that file.

  - `version` 3.1.0
  - `created` 2020-10-06
  - `id` https://doi.org/10.5904/wgms-glathida-2020-10
  - `description` Internationally collected, standardized dataset on glacier thickness from in-situ and remotely sensed observations, based on data submissions, literature review and airborne data from NASA’s Operation IceBridge.

  The latest (development) version of GlaThiDa is available from https://gitlab.com/wgms/glathida. Since the data may contain errors, we strongly suggest performing quality checks and, in case of ambiguities, to contact us as well as the original investigators and agencies. Bug reports, data submissions, and other issues should be posted to the issue tracker at https://gitlab.com/wgms/glathida/-/issues.

  GlaThiDa is a contribution to the International Association of Cryospheric Sciences (IACS) working group on glacier ice thickness estimation (https://cryosphericsciences.org/activities/ice-thickness) chaired by Daniel Farinotti, Liss Marie Andreassen and Huilin Li.
  - `publications` 
    - [1] 
      - `author` Ethan Welty, Michael Zemp, Francisco Navarro, Matthias Huss, Johannes J. Fürst, Isabelle Gärtner-Roer, Johannes Landmann, Horst Machguth, Kathrin Naegeli, Liss M. Andreassen, Daniel Farinotti, Huilin Li, and GlaThiDa Contributors
      - `title` Worldwide version-controlled database of glacier thickness observations
      - `journal` Earth System Science Data
      - `year` 2020
      - `path` https://doi.org/10.5194/essd-2020-87
    - [2] 
      - `author` Isabelle Gärtner-Roer, Kathrin Naegeli, Matthias Huss, Thomas Knecht, Horst Machguth, and Michael Zemp
      - `title` A database of worldwide glacier thickness observations
      - `journal` Global and Planetary Change
      - `year` 2014
      - `path` https://doi.org/10.1016/j.gloplacha.2014.09.003
  - `homepage` https://www.gtn-g.ch/data_catalogue_glathida
  - `profile` tabular-data-package
  - `publisher` World Glacier Monitoring Service (WGMS)
  - `temporalCoverage` 1935/2018-07-26
  - `spatialCoverage` Global
  - `languages` en
  - `licenses` 
    - [1] 
      - `title` Creative Commons Attribution 4.0 International
      - `name` CC-BY-4.0
      - `path` https://creativecommons.org/licenses/by/4.0
  - `citation` GlaThiDa Consortium (2020): Glacier Thickness Database 3.1.0. World Glacier Monitoring Service, Zurich, Switzerland. DOI:[10.5904/wgms-glathida-2020-10](https://doi.org/10.5904/wgms-glathida-2020-10)

  To cite a subset of the data, refer to the investigators and references listed in the database. For example – Dowdeswell et al. (2002), in: GlaThiDa Consortium (2020): Glacier Thickness Database 3.1.0. World Glacier Monitoring Service, Zurich, Switzerland. DOI:[10.5904/wgms-glathida-2020-10](https://doi.org/10.5904/wgms-glathida-2020-10)

  The GlaThiDa Consortium consists of the authors and contributors listed below.

## Credits

### Authors

People who have compiled and maintained GlaThiDa.

  - **Ethan Welty**, University of Colorado: Institute of Arctic and Alpine Research, United States
  - **Francisco Navarro**, Universidad Politécnica de Madrid: Escuela Técnica Superior de Ingenieros de Telecomunicación (ETSIT), Spain
  - **Johannes Fürst**, University of Erlangen–Nuremberg (FAU): Institute of Geography, Germany
  - **Isabelle Gärtner-Roer**, University of Zürich, Switzerland
  - **Johannes Landmann**, University of Zürich, Switzerland
  - **Kathrin Naegeli**, University of Fribourg, Switzerland
  - **Matthias Huss**, University of Fribourg: Laboratory of Hydraulics, Hydrology and Glaciology (VAW), Switzerland
  - **Thomas Knecht**, University of Zürich, Switzerland
  - **Horst Machguth**, University of Zürich, Switzerland
  - **Michael Zemp**, University of Zürich, Switzerland

### Contributors

People who have performed measurements, processed data, and/or submitted data to GlaThiDa, listed in alphabetical order by last name. This list does not include the authors of published literature and datasets which were added to GlaThiDa by the authors of GlaThiDa.

  - **Jakob Abermann**, Asiaq Greenland Survey, Greenland
  - **Songtao Ai**, Wuhan University, China
  - **Brian Anderson**, Victoria University of Wellington: Antarctic Research Centre, New Zealand
  - **Liss Marie Andreassen**, Norwegian Water Resources and Energy Directorate (NVE), Norway
  - **Serguei M. Arkhipov**, Russian Academy of Sciences: Institute of Geography, Russia
  - **Izumi Asaji**, Hokkaido University, Japan
  - **Andreas Bauder**, Swiss Federal Institute of Technology (ETH)–Zürich: Laboratory of Hydraulics, Hydrology and Glaciology (VAW), Switzerland
  - **Jostein Bakke**, University of Bergen: Department of Earth Sciences, Norway
  - **Toby J. Benham**, Scott Polar Research Institute, United Kingdom
  - **Douglas I. Benn**, University of Saint Andrews, United Kingdom
  - **Daniel Binder**, Central Institute for Meteorology and Geodynamics (ZAMG), Austria
  - **Elisa Bjerre**, Technical University of Denmark: Arctic Technology Centre, Denmark
  - **Helgi Björnsson**, University of Iceland, Iceland
  - **Norbert Blindow**, Institute for Geophysics, University of Münster, Germany
  - **Pascal Bohleber**, Austrian Academy of Sciences (ÖAW): Institute for Interdisciplinary Mountain Research (IGF), Austria
  - **Eliane Brändle**, University of Fribourg, Switzerland
  - **Gino Casassa**, University of Magallanes: GAIA Antarctic Research Center (CIGA), Chile
  - **Jorge Luis Ceballos**, Institute of Hydrology, Meteorology and Environmental Studies (IDEAM), Colombia
  - **Julian A. Dowdeswell**, Scott Polar Research Institute, United Kingdom
  - **Felipe Andres Echeverry Acosta**
  - **Hallgeir Elvehøy**, Norwegian Water Resources and Energy Directorate (NVE), Norway
  - **Rune Engeset**, Norwegian Water Resources and Energy Directorate (NVE), Norway
  - **Daniel Farinotti**, Swiss Federal Institute for Forest, Snow and Landscape Research (WSL), Switzerland
  - **Andrea Fischer**, Institute of Interdisciplinary Mountain Research (IGF), Austria
  - **Mauro Fischer**, University of Fribourg, Switzerland
  - **Gwenn E. Flowers**, Simon Fraser University: Department of Earth Sciences, Canada
  - **Erlend Førre**, University of Bergen: Department of Earth Sciences, Norway
  - **Yoshiyuki Fujii**, National Institute of Polar Research, Japan
  - **Johannes Fürst**, University of Erlangen–Nuremberg (FAU): Institute of Geography, Germany
  - **Isabelle Gärtner-Roer**, University of Zürich, Switzerland
  - **Mariusz Grabiec**, University of Silesia in Katowice, Poland
  - **Jon Ove Hagen**, University of Oslo, Norway
  - **Svein-Erik Hamran**, University of Oslo, Norway
  - **Lea Hartl**, Institute of Interdisciplinary Mountain Research (IGF), Austria
  - **Robert Hawley**, Dartmouth College, United States
  - **Kay Helfricht**, Institute of Interdisciplinary Mountain Research (IGF), Austria
  - **Matthias Huss**, University of Fribourg: Laboratory of Hydraulics, Hydrology and Glaciology (VAW), Switzerland
  - **Elisabeth Isaksson**, Norwegian Polar Institute, Norway
  - **Jacek Jania**, University of Silesia in Katowice, Poland
  - **Robert W. Jacobel**, Saint Olaf College: Physics Department, United States
  - **Michael Kennett**, Norwegian Water Resources and Energy Directorate (NVE), Norway
  - **Bjarne Kjøllmoen**, Norwegian Water Resources and Energy Directorate (NVE), Norway
  - **Thomas Knecht**, University of Zürich, Switzerland
  - **Jack Kohler**, Norwegian Polar Institute, Norway
  - **Vladimir Kotlyakov**, Russian Academy of Sciences: Institute of Geography, Russia
  - **Steen Savstrup Kristensen**, Technical University of Denmark: Department of Space Research and Space Technology (DTU Space), Denmark
  - **Stanislav Kutuzov**, University of Reading, United Kingdom
  - **Johannes Landmann**, University of Zürich, Switzerland
  - **Javier Lapazaran**, Universidad Politécnica de Madrid, Spain
  - **Tron Laumann**, Norwegian Water Resources and Energy Directorate (NVE), Norway
  - **Ivan Lavrentiev**, Russian Academy of Sciences: Institute of Geography, Russia
  - **Huilin Li**, Chinese Academy of Sciences: Tianshan Glaciological Station, China
  - **Katrin Lindbäck**, Norwegian Polar Institute, Norway
  - **Peter Lisager**, Asiaq Greenland Survey, Greenland
  - **Horst Machguth**, University of Zürich, Switzerland
  - **Francisco Machío**, Universidad Internacional de La Rioja (UNIR), Spain
  - **Gerhard Markl**, Institute of Interdisciplinary Mountain Research (IGF), Austria
  - **Enrico Mattea**, University of Fribourg: Department of Geography, Switzerland
  - **Kjetil Melvold**, Norwegian Water Resources and Energy Directorate (NVE), Norway
  - **Laurent Mingo**, Blue System Integration Ltd., Canada
  - **Christian Mitterer**, Institute of Interdisciplinary Mountain Research (IGF), Austria
  - **Andri Moll**, University of Zürich, Switzerland
  - **Kathrin Naegeli**, University of Fribourg, Switzerland
  - **Francisco Navarro**, Universidad Politécnica de Madrid: Escuela Técnica Superior de Ingenieros de Telecomunicación (ETSIT), Spain
  - **Ian Owens**, University of Canterbury: Department of Geography, New Zealand
  - **Finnur Pálsson**, University of Iceland, Iceland
  - **Rickard Pettersson**, Uppsala University, Sweden
  - **Rainer Prinz**, University of Graz: Department of Geography and Regional Science, Austria
  - **Ya.-M.K. Punning**, Estonian Academy of Sciences (USSR Academy of Sciences-Estonia): Institute of Geology, Estonia
  - **Antoine Rabatel**, University Grenoble Alpes, France
  - **Ian Raphael**, Dartmouth College, United States
  - **David Rippin**, University of York, United Kingdom
  - **Andrés Rivera**, Center for Scientific Studies (CECs), Chile
  - **José Luis Rodríguez Lagos**, Center for Scientific Studies (CECs), Chile
  - **John Sanders**, University of California, Berkeley: Department of Earth and Planetary Science, United States
  - **Albane Saintenoy**, University of Paris-Sud, France
  - **Arne Chr. Sætrang**, Norwegian Polar Institute, Norway
  - **Marius Schaefer**, Austral University of Chile: Institute of Physical and Mathematical Sciences (ICFM), Chile
  - **Stefan Scheiblauer**, Environmental Earth Observation Information Technology (ENVEO IT GmbH), Austria
  - **Thomas V. Schuler**, University of Oslo, Norway
  - **Heïdi Sevestre**, University of Saint Andrews, United Kingdom
  - **Bernd Seiser**, Institute of Interdisciplinary Mountain Research (IGF), Austria
  - **Ingvild Sørdal**, University of Oslo: Department of Geosciences, Norway
  - **Jakob Steiner**, University of Utrecht: Faculty of Geosciences, Netherlands
  - **Peter Alexander Stentoft**, Technical University of Denmark: Arctic Technology Centre (ARTEK), Denmark
  - **Martin Stocker-Waldhuber**, Technical University of Denmark: Arctic Technology Centre (ARTEK), Denmark
  - **Bernd Seiser**, Institute of Interdisciplinary Mountain Research (IGF), Austria
  - **Shin Sugiyama**, Hokkaido University: Institute of Low Temperature Science, Japan
  - **Rein Vaikmäe**, Tallinn University of Technology, Estonia
  - **Evgeny Vasilenko**, Academy of Sciences of Uzbekistan, Uzbekistan
  - **Nat J. Wilson**, Simon Fraser University: Department of Earth Sciences, Canada
  - **Victor S. Zagorodnov**, Russian Academy of Sciences: Institute of Geography, Russia
  - **Rodrigo Zamora**, Center for Scientific Studies (CECs), Chile
  - **Michael Zemp**, University of Zürich, Switzerland

### Sources

Published datasets incorporated into GlaThiDa, listed in order of appearance. This list should not be considered complete.

  - [Ice thickness measurements on South Tyrolean glaciers 1996-2014](https://doi.org/10.1594/PANGAEA.849390)
  - [Ground-penetrating radar (GPR) point measurements of ice thickness in Austria](https://doi.org/10.1594/PANGAEA.849497)
  - [Ice thickness of Kilimanjaro's Northern Ice Field mapped by ground-penetrating radar](https://doi.org/10.1594/PANGAEA.867908)
  - [Swiss Glacier Ice Thickness (release 2018)](https://doi.org/10.18750/icethickness.2018.r2018)
  - [Subglacial topography, ice thickness, and bathymetry of Kongsfjorden, northwestern Svalbard](https://doi.org/10.21334/npolar.2017.702ca4a7)
  - [Pre-IceBridge MCoRDS L2 Ice Thickness, Version 1](https://doi.org/10.5067/QKMTQ02C2U56)
  - [IceBridge HiCARS 1 L2 Geolocated Ice Thickness, Version 1](https://doi.org/10.5067/F5FGUT9F5089)
  - [IceBridge HiCARS 2 L2 Geolocated Ice Thickness, Version 1](https://doi.org/10.5067/9EBR2T0VXUDG)
  - [IceBridge MCoRDS L2 Ice Thickness, Version 1](https://doi.org/10.5067/GDQ0CUCVTE2Q)
  - [IceBridge PARIS L2 Ice Thickness, Version 1](https://doi.org/10.5067/OMEAKG6GIJNB)
  - [IceBridge WISE L2 Ice Thickness and Surface Elevation, Version 1](https://doi.org/10.5067/0ZBRL3GY720R)

## Data structure

The dataset is composed of three tabular data files, referred to here as `T`, `TT`, and `TTT`. The metadata describing their structure follows the Frictionless Data [Tabular Data Resource](https://frictionlessdata.io/specs/tabular-data-resource/) specification.

All data files share a common format, structure, and encoding:

  - `format` csv
  - `mediatype` text/csv
  - `encoding` utf-8
  - `profile` tabular-data-resource
  - `dialect` 
    - `header` true
    - `delimiter` ,
    - `lineTerminator` \n
    - `quoteChar` "
    - `doubleQuote` true
  - `schema` 
    - `missingValues` [""]

### `T` Glacier thickness: Overview

  - `description` Glacier-wide data and summary metadata for each ice thickness survey.
  - `path` data/T.csv
  - `schema` 
    - `primaryKey` [`GlaThiDa_ID`]

##### `GlaThiDa_ID` Survey identifier

  - `description` Unique identifier assigned by the World Glacier Monitoring Service (WGMS) to each survey. Links the corresponding entries in tables `T`, `TT`, and `TTT`.

  Note: For data submission, use your own identifier that is unique within your submitted data.
  - `type` integer
  - `constraints` 
    - `required` true
    - `unique` true

##### `POLITICAL_UNIT` Glacier country

  - `description` Two-character code (ISO 3166 Alpha-2) of the country in which the glacier is located. A list of codes is available at https://www.iso.org/obp/ui/#search/code/.
  - `type` string
  - `constraints` 
    - `required` true
    - `enum` [AF, AX, AL, DZ, AS, AD, AO, AI, AQ, AG, AR, AM, AW, AU, AT, AZ, BS, BH, BD, BB, BY, BE, BZ, BJ, BM, BT, BO, BQ, BA, BW, BV, BR, IO, BN, BG, BF, BI, CV, KH, CM, CA, KY, CF, TD, CL, CN, CX, CC, CO, KM, CD, CG, CK, CR, CI, HR, CU, CW, CY, CZ, DK, DJ, DM, DO, EC, EG, SV, GQ, ER, EE, SZ, ET, FK, FO, FJ, FI, FR, GF, PF, TF, GA, GM, GE, DE, GH, GI, GR, GL, GD, GP, GU, GT, GG, GN, GW, GY, HT, HM, VA, HN, HK, HU, IS, IN, ID, IR, IQ, IE, IM, IL, IT, JM, JP, JE, JO, KZ, KE, KI, KP, KR, KW, KG, LA, LV, LB, LS, LR, LY, LI, LT, LU, MO, MK, MG, MW, MY, MV, ML, MT, MH, MQ, MR, MU, YT, MX, FM, MD, MC, MN, ME, MS, MA, MZ, MM, NA, NR, NP, NL, NC, NZ, NI, NE, NG, NU, NF, MP, NO, OM, PK, PW, PS, PA, PG, PY, PE, PH, PN, PL, PT, PR, QA, RE, RO, RU, RW, BL, SH, KN, LC, MF, PM, VC, WS, SM, ST, SA, SN, RS, SC, SL, SG, SX, SK, SI, SB, SO, ZA, GS, SS, ES, LK, SD, SR, SJ, SE, CH, SY, TW, TJ, TZ, TH, TL, TG, TK, TO, TT, TN, TR, TM, TC, TV, UG, UA, AE, GB, UM, US, UY, UZ, VU, VE, VN, VG, VI, WF, EH, YE, ZM, ZW]

##### `GLACIER_NAME` Glacier name

  - `description` The name of the glacier, written in capital letters (A-Z).

  In order to ensure global interoperability of our dataset, glacier names should only contain the following characters: A-Z (A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z), 0-9 (0, 1, 2, 3, 4, 5, 6, 7, 8, 9), - (dash), . (period), : (colon), () (parentheses), / (forward slash), ' (apostrophe), and  (space). Characters which do not fall into the given range should be transliterated. If no Latin name exists, use the International Organization for Standardization (ISO) standards for transliteration (https://www.iso.org/ics/01.140.10/x/). If the Latin name contains accents, apply the following rules (Å → AA, Æ → AE, Ä → AE, ð → D, Ø → OE, œ → OE, Ö → OE, ß → SS, þ → TH, Ü → UE) and neglect any remaining accents.

  If a name is too long, a meaningful abbreviation should be used. In this case, the full name should be listed in `REMARKS`.
  - `type` string
  - `constraints` 
    - `maxLength` 60
    - `pattern` `[0-9A-Z\-\.:\(\)/']+( [0-9A-Z\-\.:\(\)/']+)*`

##### `GLACIER_DB` Glacier database

  - `description` The database to which `GLACIER_ID` refers, if provided.

    - GLIMS: Global Land Ice Measurements from Space
    - RGI: Randolph Glacier Inventory
    - WGI: World Glacier Inventory
    - FOG: Fluctuations of Glaciers
    - OTH: Other (full name should be listed in `REMARKS`)
  - `type` string
  - `constraints` 
    - `enum` [GLIMS, RGI, WGI, FOG, OTH]

##### `GLACIER_ID` Glacier identifier

  - `description` Identifier of the glacier in the database specified in `GLACIER_DB`. For the Randolph Glacier Inventory (RGI), the database version should be included as part of the glacier identifier (e.g. 'RGI60-07.00244'). Cannot contain double quotes (") or whitespace.
  - `type` string
  - `constraints` 
    - `maxLength` 14
    - `pattern` `[^"\s]+`

##### `LAT` Glacier latitude (°, WGS 84)

  - `description` Latitude in decimal degrees (°, WGS 84), with up to six decimal places. Positive values indicate the northern hemisphere and negative values indicate the southern hemisphere. Three decimal places may not be sufficient depending on the proximity to other glaciers.

  The point (`LAT`, `LON`) should be in the upper part of the glacier ablation area, in the main channel, and sufficiently high so as not to be lost if the glacier retreats.
  - `type` number
  - `constraints` 
    - `required` true
    - `minimum` -90
    - `maximum` 90
    - `pattern` `\-?[0-9]*(\.[0-9]{1,6})?`

##### `LON` Glacier longitude (°, WGS 84)

  - `description` Longitude in decimal degrees (°, WGS 84), with up to six decimal places. Positive values indicate east of the zero meridian and negative values indicate west of the zero meridian. Three decimal places may not be sufficient depending on the proximity to other glaciers.

  The point (`LAT`, `LON`) should be in the upper part of the glacier ablation area, in the main channel, and sufficiently high so as not to be lost if the glacier retreats.
  - `type` number
  - `constraints` 
    - `required` true
    - `minimum` -180
    - `maximum` 180
    - `pattern` `^\-?[0-9]*(\.[0-9]{1,6})?$`

##### `SURVEY_DATE` Survey date

  - `description` Date of the survey, formatted as YYYYMMDD (4-digit year, 2-digit month, and 2-digit day). Use '99' to designate unknown day or month (e.g. 20100199, 20109999). For surveys spanning multiple dates, the first date should be given and the dates further described in `REMARKS`.
  - `type` string
  - `constraints` 
    - `pattern` `[0-2][0-9]{3}(0[0-9]|1[0-2]|99)([0-2][0-9]|3[0-1]|99)`

##### `ELEVATION_DATE` Surface elevation date

  - `description` Date of the provided surface elevations (e.g. TT.LOWER_BOUND, TT.UPPER_BOUND, TTT.ELEVATION), formatted as YYYYMMDD (4-digit year, 2-digit month, and 2-digit day). Use '99' to designate unknown day or month (e.g. 20100199, 20109999). For elevations spanning multiple dates, the first date should be given and the dates further described in `REMARKS`. The source – for example, the digital elevation model (DEM) from which elevations were extracted, or whether elevations were measured with satellite navigation during the survey – should be described in `REMARKS`.
  - `type` string
  - `constraints` 
    - `pattern` `[0-2][0-9]{3}(0[0-9]|1[0-2]|99)([0-2][0-9]|3[0-1]|99)`

##### `AREA` Total area (km^2)

  - `description` Total glacier area (km^2), with up to five decimal places. If the date for the area is different from `ELEVATION_DATE`, it should be noted in `REMARKS`.
  - `type` number
  - `constraints` 
    - `minimum` 0
    - `pattern` `[0-9]*(\.[0-9]{1,5})?`

##### `MEAN_SLOPE` Mean glacier slope (°)

  - `description` Mean surface slope over the entire glacier (°), as an integer. If the date for the slope is different from `ELEVATION_DATE`, it should be noted in `REMARKS`.
  - `type` integer
  - `constraints` 
    - `minimum` 0
    - `maximum` 90

##### `MEAN_THICKNESS` Mean glacier thickness (m)

  - `description` Mean ice thickness (m), as an integer. Ideally, this represents the interpolated mean over the entire glacier rather than the mean of the individual thickness measurements. Otherwise, it should be noted in `REMARKS` and `DATA_FLAG` should be set to '2'.
  - `type` integer
  - `constraints` 
    - `minimum` 0
    - `maximum` 9999

##### `MEAN_THICKNESS_UNCERTAINTY` Uncertainty of mean glacier thickness (m)

  - `description` Estimated random error of `MEAN_THICKNESS` (m), as an integer.
  - `type` integer
  - `constraints` 
    - `minimum` 0
    - `maximum` 9999

##### `MAXIMUM_THICKNESS` Maximum glacier thickness (m)

  - `description` Maximum ice thickness (m), as an integer.
  - `type` integer
  - `constraints` 
    - `minimum` 0
    - `maximum` 9999

##### `MAX_THICKNESS_UNCERTAINTY` Uncertainty of maximum glacier thickness (m)

  - `description` Estimated random error of `MAXIMUM_THICKNESS` (m), as an integer.
  - `type` integer
  - `constraints` 
    - `minimum` 0
    - `maximum` 9999

##### `SURVEY_METHOD` Survey method

  - `description` Survey method used.

    - DRI: Drilling
    - DRIh: Drilling (hydrothermal)
    - DRIm: Drilling (mechanical)
    - GPR: Ground penetrating radar
    - GPRa: Ground penetrating radar (airborne)
    - GPRt: Ground penetrating radar (terrestrial)
    - GEL: Geoelectric
    - EM: Electromagnetic
    - HYM: Hydrometric
    - SEI: Seismic
    - OTH: Other (should be described in `SURVEY_METHOD_DETAILS`)
  - `type` string
  - `constraints` 
    - `enum` [DRI, DRIh, DRIm, GPR, GPRa, GPRt, GEL, EM, HYM, SEI, OTH]

##### `SURVEY_METHOD_DETAILS` Survey method details

  - `description` Details useful to assess the uncertainty of the ice thickness measurements. For example, 'GPR full-range system, 100-MHz shielded antenna, constant wave velocity in ice of 0.168 m per ns.' Cannot contain double quotes (") or whitespace other than space.
  - `type` string
  - `constraints` 
    - `pattern` `[^"\s]+( [^"\s]+)*`

##### `NUMBER_OF_SURVEY_POINTS` Number of survey points

  - `description` Total number of survey points taken. Should be equal to either the number of points in table `TTT`, the number of points originally surveyed, or the number of points used to estimate the mean thickness over the glacier (`T.MEAN_THICKNESS`) or elevation bands (`TT.MEAN_THICKNESS`). The specific meaning should be given in `REMARKS`.
  - `type` integer
  - `constraints` 
    - `minimum` 0

##### `NUMBER_OF_SURVEY_PROFILES` Number of survey profiles

  - `description` Total number of survey profiles taken. Should be equal to either the number of listed profiles (i.e. the number of unique values of `TTT.PROFILE_ID`), the number of profiles originally surveyed, or the number of profiles used to estimate the mean thickness over the glacier (`T.MEAN_THICKNESS`) or elevation bands (`TT.MEAN_THICKNESS`). The specific meaning should be given in `REMARKS`.
  - `type` integer
  - `constraints` 
    - `minimum` 0

##### `TOTAL_LENGTH_OF_SURVEY_PROFILES` Total length of survey profiles (km)

  - `description` Total length of survey profiles taken (km), with up to two decimal places. Should be equal to either the total length of the listed profiles (i.e. the sum of the lengths of each profile, `TTT.PROFILE_ID`, based on the coordinates of the points, `TTT.POINT_LAT` and `TTT.POINT_LON`), the total length of the profiles originally surveyed, or the total length of the profiles used to estimate the mean thickness over the glacier (`T.MEAN_THICKNESS`) or elevation bands (`TT.MEAN_THICKNESS`). The specific meaning should be given in `REMARKS`.
  - `type` number
  - `constraints` 
    - `minimum` 0
    - `pattern` `[0-9]*(\.[0-9]{1,2})?`

##### `INTERPOLATION_METHOD` Interpolation method

  - `description` Interpolation method used to extrapolate ice thickness from survey points to the entire glacier.

    - IDW: Inverse distance weighting
    - KRG: Kriging
    - ANU: ANUDEM, including ArcInfo TOPOGRID and ArcGIS Topo To Raster)
    - TRI: Triangulation, including Triangulated irregular network (TIN)
    - OTH: Other (should be described in `REMARKS`)
  - `type` string
  - `constraints` 
    - `enum` [IDW, KRG, ANU, TRI, OTH]

##### `INVESTIGATOR` Investigators

  - `description` Name of the people or agencies that performed the survey or processed the data. For people, both first and last name should be given, complemented by other identifiers such as their affiliation or ORCID (http://orcid.org). Cannot contain double quotes (") or whitespace other than space.
  - `type` string
  - `constraints` 
    - `pattern` `[^"\s]+( [^"\s]+)*`

##### `SPONSORING_AGENCY` Sponsoring agencies

  - `description` Name and location of the agencies that sponsored the survey or hold the data. Cannot contain double quotes (") or whitespace other than space.
  - `type` string
  - `constraints` 
    - `pattern` `[^"\s]+( [^"\s]+)*`

##### `REFERENCES` References

  - `description` References to published literature directly relating to the survey, including the Digital Object Identifier (DOI) or URL when available. Cannot contain double quotes (") or whitespace other than space.
  - `type` string
  - `constraints` 
    - `pattern` `[^"\s]+( [^"\s]+)*`

##### `DATA_FLAG` Data flag

  - `description` Whether glacier thickness is erroneous or limited to parts of the glacier. All issues should be described in `REMARKS`.

    - 1: Erroneous glacier thickness
    - 2: Glacier thickness limited to parts of glacier
    - 3: Other issue
  - `type` integer
  - `constraints` 
    - `enum` [1, 2, 3]

##### `REMARKS` Remarks

  - `description` Any other important information about the survey not included elsewhere. Cannot contain double quotes (") or whitespace other than space.
  - `type` string
  - `constraints` 
    - `pattern` `[^"\s]+( [^"\s]+)*`

### `TT` Glacier thickness: By elevation band

  - `description` Glacier thickness by bands of surface elevation, typically derived from maps of ice thickness.
  - `path` data/TT.csv
  - `schema` 
    - `uniqueKeys` 
      - [1] [`GlaThiDa_ID`, `LOWER_BOUND`, `UPPER_BOUND`]
    - `foreignKeys` 
      - [1] 
        - `fields` [`GlaThiDa_ID`, `POLITICAL_UNIT`, `GLACIER_NAME`]
        - `reference` 
          - `resource` `t`
          - `fields` [`GlaThiDa_ID`, `POLITICAL_UNIT`, `GLACIER_NAME`]

##### `GlaThiDa_ID` Survey identifier

  - `description` Unique identifier assigned by the World Glacier Monitoring Service (WGMS) to each survey. Links the corresponding entries in tables `T`, `TT`, and `TTT`.

  Note: For data submission, use your own identifier that is unique within your submitted data.
  - `type` integer
  - `constraints` 
    - `required` true

##### `POLITICAL_UNIT` Glacier country

  - `description` Two-character code (ISO 3166 Alpha-2) of the country in which the glacier is located. A list of codes is available at https://www.iso.org/obp/ui/#search/code/.
  - `type` string
  - `constraints` 
    - `required` true
    - `enum` [AF, AX, AL, DZ, AS, AD, AO, AI, AQ, AG, AR, AM, AW, AU, AT, AZ, BS, BH, BD, BB, BY, BE, BZ, BJ, BM, BT, BO, BQ, BA, BW, BV, BR, IO, BN, BG, BF, BI, CV, KH, CM, CA, KY, CF, TD, CL, CN, CX, CC, CO, KM, CD, CG, CK, CR, CI, HR, CU, CW, CY, CZ, DK, DJ, DM, DO, EC, EG, SV, GQ, ER, EE, SZ, ET, FK, FO, FJ, FI, FR, GF, PF, TF, GA, GM, GE, DE, GH, GI, GR, GL, GD, GP, GU, GT, GG, GN, GW, GY, HT, HM, VA, HN, HK, HU, IS, IN, ID, IR, IQ, IE, IM, IL, IT, JM, JP, JE, JO, KZ, KE, KI, KP, KR, KW, KG, LA, LV, LB, LS, LR, LY, LI, LT, LU, MO, MK, MG, MW, MY, MV, ML, MT, MH, MQ, MR, MU, YT, MX, FM, MD, MC, MN, ME, MS, MA, MZ, MM, NA, NR, NP, NL, NC, NZ, NI, NE, NG, NU, NF, MP, NO, OM, PK, PW, PS, PA, PG, PY, PE, PH, PN, PL, PT, PR, QA, RE, RO, RU, RW, BL, SH, KN, LC, MF, PM, VC, WS, SM, ST, SA, SN, RS, SC, SL, SG, SX, SK, SI, SB, SO, ZA, GS, SS, ES, LK, SD, SR, SJ, SE, CH, SY, TW, TJ, TZ, TH, TL, TG, TK, TO, TT, TN, TR, TM, TC, TV, UG, UA, AE, GB, UM, US, UY, UZ, VU, VE, VN, VG, VI, WF, EH, YE, ZM, ZW]

##### `GLACIER_NAME` Glacier name

  - `description` The name of the glacier, written in capital letters (A-Z).

  In order to ensure global interoperability of our dataset, glacier names should only contain the following characters: A-Z (A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z), 0-9 (0, 1, 2, 3, 4, 5, 6, 7, 8, 9), - (dash), . (period), : (colon), () (parentheses), / (forward slash), ' (apostrophe), and  (space). Characters which do not fall into the given range should be transliterated. If no Latin name exists, use the International Organization for Standardization (ISO) standards for transliteration (https://www.iso.org/ics/01.140.10/x/). If the Latin name contains accents, apply the following rules (Å → AA, Æ → AE, Ä → AE, ð → D, Ø → OE, œ → OE, Ö → OE, ß → SS, þ → TH, Ü → UE) and neglect any remaining accents.

  If a name is too long, a meaningful abbreviation should be used. In this case, the full name should be listed in `REMARKS`.
  - `type` string
  - `constraints` 
    - `maxLength` 60
    - `pattern` `[0-9A-Z\-\.:\(\)/']+( [0-9A-Z\-\.:\(\)/']+)*`

##### `SURVEY_DATE` Survey date

  - `description` Date that the ice thickness was surveyed in the elevation band, formatted as YYYYMMDD (4-digit year, 2-digit month, and 2-digit day). Use '99' to designate unknown day or month (e.g. 20100199, 20109999). For surveys spanning multiple dates, the first date should be given and the dates further described in `REMARKS`.
  - `type` string
  - `constraints` 
    - `pattern` `[0-2][0-9]{3}(0[0-9]|1[0-2]|99)([0-2][0-9]|3[0-1]|99)`

##### `LOWER_BOUND` Lower elevation bound (m)

  - `description` Lower boundary of the surface elevation band (m), as an integer. Elevations should be relative to mean sea level (geoid) unless noted in `REMARKS` or `T.REMARKS`.
  - `type` integer
  - `constraints` 
    - `required` true
    - `maximum` 9999

##### `UPPER_BOUND` Upper elevation bound (m)

  - `description` Upper boundary of the surface elevation band (m), as an integer. Elevations should be relative to mean sea level (geoid) unless noted in `REMARKS` or `T.REMARKS`.
  - `type` integer
  - `constraints` 
    - `required` true
    - `maximum` 9999

##### `AREA` Total band area (km^2)

  - `description` Total glacier area (km^2), with up to five decimal places, for the elevation band. If the date for the area is different from `ELEVATION_DATE`, it should be noted in `REMARKS`.
  - `type` number
  - `constraints` 
    - `minimum` 0
    - `pattern` `[0-9]*(\.[0-9]{1,5})?`

##### `MEAN_SLOPE` Mean band slope (°)

  - `description` Mean surface slope (°), as an integer, for the elevation band. If the date for the slope is different from `ELEVATION_DATE`, it should be noted in `REMARKS`.
  - `type` integer
  - `constraints` 
    - `minimum` 0
    - `maximum` 90

##### `MEAN_THICKNESS` Mean band thickness (m)

  - `description` Mean ice thickness (m), as an integer, for the elevation band. Ideally, this is the interpolated mean of the entire elevation band rather than the mean of the individual thickness measurements. Otherwise, it should be noted in `REMARKS` and `DATA_FLAG` should be set to '2'.
  - `type` integer
  - `constraints` 
    - `required` true
    - `minimum` 0
    - `maximum` 9999

##### `MEAN_THICKNESS_UNCERTAINTY` Uncertainty of mean band thickness (m)

  - `description` Estimated random error of `MEAN_THICKNESS` (m), as an integer.
  - `type` integer
  - `constraints` 
    - `minimum` 0
    - `maximum` 9999

##### `MAXIMUM_THICKNESS` Maximum band thickness (m)

  - `description` Maximum ice thickness (m), as an integer, for the elevation band.
  - `type` integer
  - `constraints` 
    - `minimum` 0
    - `maximum` 9999

##### `MAX_THICKNESS_UNCERTAINTY` Uncertainty of maximum band thickness (m)

  - `description` Estimated random error of `MAXIMUM_THICKNESS` (m), as an integer.
  - `type` integer
  - `constraints` 
    - `minimum` 0
    - `maximum` 9999

##### `DATA_FLAG` Data flag

  - `description` Whether ice thickness is erroneous or limited to parts of the elevation band. Issues specific to the elevation band should be described in `REMARKS` while issues common to all elevation bands should be described in `T.REMARKS`.

    - 1: Erroneous ice thickness
    - 2: Ice thickness limited to parts of the elevation band
    - 3: Other issue
  - `type` integer
  - `constraints` 
    - `enum` [1, 2, 3]

##### `REMARKS` Remarks

  - `description` Any other important information about the survey – specific to the elevation band – not included elsewhere. Cannot contain double quotes (") or whitespace other than space.
  - `type` string
  - `constraints` 
    - `pattern` `[^"\s]+( [^"\s]+)*`

### `TTT` Glacier thickness: Point measurements

  - `description` Glacier thickness measured at specific points.
  - `path` data/TTT.csv
  - `schema` 
    - `uniqueKeys` 
      - [1] [`GlaThiDa_ID`, `PROFILE_ID`, `POINT_ID`]
    - `foreignKeys` 
      - [1] 
        - `fields` [`GlaThiDa_ID`, `POLITICAL_UNIT`, `GLACIER_NAME`]
        - `reference` 
          - `resource` `t`
          - `fields` [`GlaThiDa_ID`, `POLITICAL_UNIT`, `GLACIER_NAME`]

##### `GlaThiDa_ID` Survey identifier

  - `description` Unique identifier assigned by the World Glacier Monitoring Service (WGMS) to each survey. Links the corresponding entries in tables `T`, `TT`, and `TTT`.

  Note: For data submission, use your own identifier that is unique within your submitted data.
  - `type` integer
  - `constraints` 
    - `required` true

##### `POLITICAL_UNIT` Glacier country

  - `description` Two-character code (ISO 3166 Alpha-2) of the country in which the glacier is located. A list of codes is available at https://www.iso.org/obp/ui/#search/code/.
  - `type` string
  - `constraints` 
    - `required` true
    - `enum` [AF, AX, AL, DZ, AS, AD, AO, AI, AQ, AG, AR, AM, AW, AU, AT, AZ, BS, BH, BD, BB, BY, BE, BZ, BJ, BM, BT, BO, BQ, BA, BW, BV, BR, IO, BN, BG, BF, BI, CV, KH, CM, CA, KY, CF, TD, CL, CN, CX, CC, CO, KM, CD, CG, CK, CR, CI, HR, CU, CW, CY, CZ, DK, DJ, DM, DO, EC, EG, SV, GQ, ER, EE, SZ, ET, FK, FO, FJ, FI, FR, GF, PF, TF, GA, GM, GE, DE, GH, GI, GR, GL, GD, GP, GU, GT, GG, GN, GW, GY, HT, HM, VA, HN, HK, HU, IS, IN, ID, IR, IQ, IE, IM, IL, IT, JM, JP, JE, JO, KZ, KE, KI, KP, KR, KW, KG, LA, LV, LB, LS, LR, LY, LI, LT, LU, MO, MK, MG, MW, MY, MV, ML, MT, MH, MQ, MR, MU, YT, MX, FM, MD, MC, MN, ME, MS, MA, MZ, MM, NA, NR, NP, NL, NC, NZ, NI, NE, NG, NU, NF, MP, NO, OM, PK, PW, PS, PA, PG, PY, PE, PH, PN, PL, PT, PR, QA, RE, RO, RU, RW, BL, SH, KN, LC, MF, PM, VC, WS, SM, ST, SA, SN, RS, SC, SL, SG, SX, SK, SI, SB, SO, ZA, GS, SS, ES, LK, SD, SR, SJ, SE, CH, SY, TW, TJ, TZ, TH, TL, TG, TK, TO, TT, TN, TR, TM, TC, TV, UG, UA, AE, GB, UM, US, UY, UZ, VU, VE, VN, VG, VI, WF, EH, YE, ZM, ZW]

##### `GLACIER_NAME` Glacier name

  - `description` The name of the glacier, written in capital letters (A-Z).

  In order to ensure global interoperability of our dataset, glacier names should only contain the following characters: A-Z (A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z), 0-9 (0, 1, 2, 3, 4, 5, 6, 7, 8, 9), - (dash), . (period), : (colon), () (parentheses), / (forward slash), ' (apostrophe), and  (space). Characters which do not fall into the given range should be transliterated. If no Latin name exists, use the International Organization for Standardization (ISO) standards for transliteration (https://www.iso.org/ics/01.140.10/x/). If the Latin name contains accents, apply the following rules (Å → AA, Æ → AE, Ä → AE, ð → D, Ø → OE, œ → OE, Ö → OE, ß → SS, þ → TH, Ü → UE) and neglect any remaining accents.

  If a name is too long, a meaningful abbreviation should be used. In this case, the full name should be listed in `REMARKS`.
  - `type` string
  - `constraints` 
    - `maxLength` 60
    - `pattern` `[0-9A-Z\-\.:\(\)/']+( [0-9A-Z\-\.:\(\)/']+)*`

##### `SURVEY_DATE` Survey date

  - `description` Date that the point was surveyed, formatted as YYYYMMDD (4-digit year, 2-digit month, and 2-digit day). Use '99' to designate unknown day or month (e.g. 20100199, 20109999).
  - `type` string
  - `constraints` 
    - `pattern` `[0-2][0-9]{3}(0[0-9]|1[0-2]|99)([0-2][0-9]|3[0-1]|99)`

##### `PROFILE_ID` Profile identifier

  - `description` Identifier for the survey profile which the point belongs to (if applicable). Should serve to both distinguish between different profiles and to sort the profiles in the order in which they were collected (e.g. 1, 2, 3, ...).
  - `type` string
  - `constraints` 
    - `maxLength` 8

##### `POINT_ID` Point identifier

  - `description` Identifier for the point. Should serve to both distinguish between different points and to sort the profiles in the order in which they were collected (e.g. 1, 2, 3, ...).
  - `type` string
  - `constraints` 
    - `required` true
    - `maxLength` 8

##### `POINT_LAT` Point latitude (°, WGS 84)

  - `description` Latitude in decimal degrees (°, WGS 84), with up to seven decimal places. Positive values indicate the northern hemisphere and negative values indicate the southern hemisphere.
  - `type` number
  - `constraints` 
    - `required` true
    - `minimum` -90
    - `maximum` 90
    - `pattern` `\-?[0-9]*(\.[0-9]{1,7})?`

##### `POINT_LON` Point longitude (°, WGS 84)

  - `description` Longitude in decimal degrees (°, WGS 84), with up to seven decimal places. Positive values indicate east of the zero meridian and negative values indicate west of the zero meridian.
  - `type` number
  - `constraints` 
    - `required` true
    - `minimum` -180
    - `maximum` 180
    - `pattern` `\-?[0-9]*(\.[0-9]{1,7})?`

##### `ELEVATION` Point elevation (m)

  - `description` Point elevation (m), as an integer. Elevations should be relative to mean sea level (geoid) unless noted in `REMARKS` or `T.REMARKS`.
  - `type` integer
  - `constraints` 
    - `maximum` 999999

##### `THICKNESS` Ice thickness (m)

  - `description` Ice thickness (m), as an integer, measured at the point.
  - `type` integer
  - `constraints` 
    - `required` true
    - `minimum` 0
    - `maximum` 9999

##### `THICKNESS_UNCERTAINTY` Uncertainty of ice thickness (m)

  - `description` Estimated random error of `THICKNESS` (m), as an integer.
  - `type` integer
  - `constraints` 
    - `minimum` 0
    - `maximum` 9999

##### `DATA_FLAG` Data flag

  - `description` Whether ice thickness is erroneous. Issues specific to the point should be described in `REMARKS` while issues common to all points in the survey should be described in `T.REMARKS`.

    - 1: Erroneous ice thickness
    - 3: Other issue
  - `type` integer
  - `constraints` 
    - `enum` [1, 3]

##### `REMARKS` Remarks

  - `description` Any other important information about the survey – specific to the point – not included elsewhere. Cannot contain double quotes (") or whitespace other than space.
  - `type` string
  - `constraints` 
    - `pattern` `[^"\s]+( [^"\s]+)*`
